FROM abiosoft/caddy:php-no-stats
ENV ACME_AGREE=true SCHEME=http HOST=0.0.0.0 EMAIL=off USERNAME=user PASSWORD=password
COPY --chown=www-user src/ /srv
COPY Caddyfile /etc/Caddyfile
RUN chown -R www-user:www-user /srv
