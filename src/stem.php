<?php
/**
 * Created by PhpStorm.
 * User: stan
 * Date: 6/27/18
 * Time: 4:58 PM
 */
function getNextVote($votes, $vote_id){
    $i = array_search($vote_id, array_keys($votes)) + 1;
    if($i == count(array_keys($votes))){
        return "__DEADEND";
    }else{
        return array_keys($votes)[$i];
    }
}
function titleToVariableName($title){
    $title = preg_replace('/[^a-zA-Z0-9 ]/i', '', $title);
    return "[" . str_replace(" ", "_", strtoupper($title)) . "]";
}
function titleToPostName($title){
    $title = preg_replace('/[^a-zA-Z0-9 ]/i', '', $title);
    return str_replace(" ", "_", strtoupper($title));
}
function createListVote($votes, $vote_id, $token){
    $vote = $votes[$vote_id];
    $html = "";
    $html .= "<form method='post' action='stem.php'>";
    $html .= "<input type='hidden' value='". getNextVote($votes, $vote_id) . "' name='vote_id'>";
    $html .= "<input type='hidden' value='". $vote_id . "' name='current_vote_id'>";
    $html .= "<input type='hidden' value='". $token . "' name='token'>";
    $html .= "<h2>". $vote['title'] . "</h2>";
    foreach ($vote['options'] as $option){
        $html .= "<div class='form-group form-check'>";
        $html .= "<input type='radio' class='form-check-input' id='". $option . "' name='votecast" . titleToVariableName($vote['title']) . "' value='". $option . "'";
        if($option == "Onthouden"){
            $html .= " checked >";
        }else{
            $html .= ">";
        }
        $html .= "<label class='form-check-label radio-syntax' for='". $option."' >" . $option . "</label>";
        $html .= "</div>";
    }
    $html .= "<input type='submit' value='Volgende Stemming' class='btn btn-primary float-right'>";
    $html .= "</form>";
    return $html;
}

function createGridVote($votes, $vote_id, $token){
    $vote = $votes[$vote_id];
    $html = "";
    $html .= "<form method='post' action='stem.php'>";
    $html .= "<input type='hidden' value='". getNextVote($votes, $vote_id) . "' name='vote_id'>";
    $html .= "<input type='hidden' value='". $vote_id . "' name='current_vote_id'>";
    $html .= "<input type='hidden' value='". $token . "' name='token'>";
    $html .= "<h2>". $vote['title'] . "</h2>";
    $html .= "<table class='table table-responsive'>";
    $html .= "<thead>";
    $html .= "<tr>";
    $html .= "<th>Optie</th>";
    foreach ($vote['options_horizontal'] as $option){
        $html .= "<th>".$option."</th>";
    }
    $html .= "</tr>";
    $html .= "</thead>";
    $html .= "<tbody>";
    foreach ($vote['options_vertical'] as $option){
        $html .= "<tr>";
        $html .= "<td>".$option."</td>";
        foreach ($vote['options_horizontal'] as $option2){
            $html .= "<td>";
            $html .= "<input type='radio' value='" . $option2 ."' name='votecast" . titleToVariableName($vote['title']) . titleToVariableName($option). "'";
            if($option2 == "Onthouden"){
                $html .= " checked >";
            }else{
                $html .= ">";
            }
            $html .= "</td>";
        }
        $html .= "</tr>";
    }
    $html .= "</tbody>";
    $html .= "</table>";
    $html .= "<input type='submit' value='Volgende Stemming' class='btn btn-primary float-right'>";
    $html .= "</form>";
    return $html;
}

function handleVote($votes, $vote_id)
{
    $vote = $votes[$vote_id];
    if($vote['status'] === "closed"){
        return;
    }
    $data = $_POST["votecast"];
    file_put_contents("votes/vote_" . $vote_id . "_" . titleToPostName($_POST['token']), json_encode($data[titleToPostName($vote['title'])]));
}

$string = file_get_contents("votes.json");
$votes = json_decode($string, true);

// Remove the helper spaces every 4th character.
if (isset($_POST['token'])) {
    $_POST['token'] = str_replace(' ', '', $_POST['token']);
}

if(isset($_POST['vote_id']) && (array_key_exists($_POST['vote_id'], $votes) || $_POST['vote_id'] == "__DEADEND")){
    $vote_id = $_POST['vote_id'];
    $token = isset($_POST['token']) ? htmlspecialchars($_POST['token']) : die("This is not supposed to happen (ERROR_ID_1)");
    if(array_search($token, json_decode(file_get_contents("tokens.json"))) === false){
        // token is niet bekend, ga terug naar de index.
        header('Location: /?error=token_not_found');
        die();
    }
    $vote = $votes[$vote_id];
    if ($vote['status'] == "closed"){
        $html = "<h2>" . $vote['title'] . "</h2>";
        $html .= '<div class="alert alert-warning" role="alert">Deze stemming is (nog) gesloten.</div><br>';

        // Form to reload the current vote.
        $html .= "<form method='post' action='stem.php'>";
        $html .= "<input type='hidden' value='". $vote_id . "' name='vote_id'>";
        $html .= "<input type='hidden' value='". $token . "' name='token'>";
        $html .= "<input type='submit' value='Verversen' class='btn btn-primary float-right'>";
        $html .= "</form>";

        // Form to go to the next vote.
        $html .= "<form method='post' action='stem.php'>";
        $html .= "<input type='hidden' value='". getNextVote($votes, $vote_id) . "' name='vote_id'>";
        $html .= "<input type='hidden' value='". $token . "' name='token'>";
        $html .= "<input type='submit' value='Stemming Overslaan' class='btn btn-default float-left'>";
        $html .= "</form>";

        $vote_html = $html;
    }else{
        if ($vote['type'] === "list"){
            //list vote
            $vote_html = createListVote($votes, $vote_id, $token);
        }elseif ($vote['type'] === "grid"){
            //grid vote
            $vote_html = createGridVote($votes, $vote_id, $token);
        }elseif ($vote_id == "__DEADEND"){
            //end of vote
            $vote_html = '
            <div class="alert alert-success" role="alert">
                <h4 class="alert-heading">Dit was de laatste stemming.</h4>
                <p>We willen je bedanken voor het uitbrengen van jouw stem als lid van de vereniging!</p>
                <hr>
                <p class="mb-0">
                    Deze stemsoftware is <a href="https://gitlab.com/syntaxleiden/vote-system" class="alert-link" target="_blank">open-source</a> beschikbaar.
                    Stemresultaten worden na afloop van de ALV gepubliceerd op het
                    <a href="https://leden.syntaxleiden.nl" class="alert-link" target="_blank">
                        ledenportal</a>.
                </p>
            </div>
            ';
        }else{
            throw new UnexpectedValueException("This is not supposed to happen (ERROR_ID_2)");
        }
    }
    if(isset($_POST['current_vote_id']) && array_key_exists($_POST['current_vote_id'], $votes)){
        handleVote($votes, $_POST['current_vote_id']);
    }
}else{
    header("Location: index.php");
}
?>

<?php
/**
 * Created by PhpStorm.
 * User: stan
 * Date: 6/27/18
 * Time: 4:20 PM
 */
?>
<!--
    Copyright (C) 2018  Stan Overgauw

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">

    <title>Syntax ALV - Digitaal Stemmen</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.typekit.net/qvc3lcx.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<nav class="navbar navbar-light syntax-header">
    <span class="navbar-brand" href="#">
        <img src="https://syntaxleiden.nl/img/logo-syntax.png" class="d-inline-block align-top" alt="">
        Digitaal Stemmen
    </span>
</nav>
<div class="container syntax-content">
    <div class="row">
        <div class="col">
            <p>
                Welkom op de website voor het digitaal stemmen van <a href="https://syntaxleiden.nl" target="_blank">S.V. Syntax</a>. Alle stemmen zijn anoniem.
            </p>
            <p>
                Je unieke token is: <strong class="readable-token"><?php echo chunk_split($token, 4, ' '); ?></strong>
            </p>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">
            <?php

            echo $vote_html;

            ?>
        </div>
    </div>
</div>
</body>
</html>
