<?php
function titleToPostName($title){
    $title = preg_replace('/[^a-zA-Z0-9 ]/i', '', $title);
    return str_replace(" ", "_", strtoupper($title));
}
function countVotesList($files, $vote){
    $options = $vote['options'];
    $options_counted = array();
    $count = 0;
    foreach ($options as $option){
        $options_counted[$option] = 0;
        foreach ($files as $file){
            if($option == json_decode(file_get_contents($file))){
                $options_counted[$option] += 1;
                if(json_decode(file_get_contents($file)) != "Onthouden"){
                    $count += 1;
                }
            }

        }
    }
    $html = "<table class='table'><tr><th>Stelling</th><th>Telling</th><th>Percentage</th></tr>";
    foreach (array_keys($options_counted) as $key){
        if($key == "Onthouden" || $options_counted[$key] == 0){
            $html .= "<tr><td>" . $key . "</td><td>" . $options_counted[$key] . "</td><td></td></tr>";
        }else{
            $html .= "<tr><td>" . $key . "</td><td>" . $options_counted[$key] . "</td><td>" .$options_counted[$key]/$count * 100 . "%</td></tr>";
        }
    }
    $html .= "<tr><td><strong>Totaal</strong></td><td><strong>".count($files)."</strong></td><td></td></tr>";
    $html .= "</table>";
    return $html;
}
function countVotesGrid($files, $vote){
    $options_counted = array();
    foreach ($vote['options_vertical'] as $option){
        $option = titleToPostName($option);
        $options_counted[$option] = array();
        foreach ($vote['options_horizontal'] as $option2){
            $options_counted[$option][$option2] = 0;
        }
        $count = 0;
        foreach ($files as $file){
            foreach ($vote['options_horizontal'] as $option2) {
                $chosenOption = json_decode(file_get_contents($file), true)[$option];
                if($option2 == $chosenOption){
                    $options_counted[$option][$option2] += 1;
                    if($chosenOption != "ONTHOUDEN"){	//TODO: Make it possible to change orientation of grid.
                        $count += 1;
                    }
                }
            }
        }
        $options_counted[$option]["Totaal"] = $count;
    }

    $html = "<table class='table'>";
    $html .= "<tr><th>Optie</th>";
    foreach (array_keys($options_counted[array_keys($options_counted)[0]]) as $header){
        $html .= "<th>" . $header . "</th>";
    }
    $html .= "</tr>";

    foreach (array_keys($options_counted) as $key){
        $html .= "<tr>";
        $html .= "<td>" . $key ."</td>";
        foreach (array_keys($options_counted[$key]) as $count_key){
            $count_value = $options_counted[$key][$count_key];
            $count_total = $options_counted[$key]['Totaal'];
            if($count_value === 0 ){
                $html .= "<td>0</td>";
            }elseif ($count_key == "Onthouden" || $count_key == "Totaal"){
                $html .= "<td>".$count_value."</td>";
            }else{
                $html .= "<td>" . $count_value . " - " . round($count_value/$count_total * 100, 2) . "%</td>";
            }
        }
        $html .= "</tr>";
    }
    $html .="</table>";
    return $html;
}
?>

<!--
    Copyright (C) 2018  Stan Overgauw

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Digitaal stemmen - Admin</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.typekit.net/qvc3lcx.css">
    <link rel="stylesheet" href="/style.css">

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light syntax-header">
    <span class="navbar-brand" href="#">
        <img src="https://syntaxleiden.nl/img/logo-syntax.png" class="d-inline-block align-top" alt="">
        Digitaal Stemmen &dash; Admin
    </span>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbar">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/admin">Overzicht</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/votes.php">Stemmingen</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/tokens.php">Tokens</a>
            </li>
        </ul>
    </div>
</nav>
<div class="container syntax-content">
    <div class="row">
        <div class="col">

            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Let op</strong> &mdash; Ga niet met meer dan één gebruiker tegelijk stemmingen openen, sluiten of aan passen.
                Alle stemming instellingen worden in één bestand worden opgeslagen. Met meerdere gebruikers gaan er dus instellingen verloren.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <?php
            $string = file_get_contents("../votes.json");
            $votes = json_decode($string, true);

            if(count($votes) < 1) {
                echo "<center><i>Er zijn nog geen stemmingen om te laten zien.</i></center>";
            }

            foreach (array_keys($votes) as $key){
                $title = $votes[$key]['title'];
                $status = $votes[$key]['status'];
                $votes_counted = glob("../votes/vote_" . $key . "_*");

                $status_html = '<span class="float-right badge badge-'.$status.'">'.$status.'</span>';

                echo "<h3>$title &nbsp; $status_html</h3>";
                if($votes[$key]['type'] == "list"){
                    echo countVotesList($votes_counted, $votes[$key]);
                }elseif ($votes[$key]['type'] == "grid"){
                    echo countVotesGrid($votes_counted, $votes[$key]);
                }else{
                    echo "Not supposed to happen (ERROR_ID_3)";
                }
            }
            ?>
        </div>
    </div>
    <br>
    <br>
    <br><br>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</body>
</html>
