<!--
    Copyright (C) 2018  Stan Overgauw
    Modified by Sander Laarhoven

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<?php
$string = file_get_contents("../votes.json");
$votes = json_decode($string, true);


?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.typekit.net/qvc3lcx.css">
    <link rel="stylesheet" href="/style.css">
    <title>Digitaal stemmen - Admin</title>
</head>
<body>
<nav class="navbar navbar-light syntax-header">
    <span class="navbar-brand" href="#">
        <img src="https://syntaxleiden.nl/img/logo-syntax.png" class="d-inline-block align-top" alt="">
        Digitaal Stemmen &dash; Admin
    </span>
</nav>
<div class="container">
    <div class="row">
        <div class="col">
            <p></p>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a href="/admin" class="btn btn-light float-left">Terug naar Overzicht</a>
            &nbsp;
            <a href="/admin/export_votes.php" class="btn btn-light">Exporteer Resultaten</a>
            <a href="/admin/edit_vote.php?id=new" class="btn btn-success float-right">Nieuwe Stemming</a>
            <br><br><br>
            <p>
                <?php
                if(count($votes) < 1) {
                    echo "<center><i>Er zijn nog geen stemmingen om te laten zien.</i></center>";
                }
                ?>
            </p>
            <div class="list-group">
                <?php
                foreach ($votes as $vote_key => $vote) {
                ?>
                <a href="/admin/edit_vote.php?id=<?php echo $vote_key; ?>" class="list-group-item list-group-item-action">
                    <?php echo $vote['title']; ?>
                    <span class="float-right badge badge-<?php echo $vote['status']; ?>"><?php echo $vote['status']; ?></span>
                </a>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</body>
</html>
