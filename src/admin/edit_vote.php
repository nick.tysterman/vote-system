<!--
    Copyright (C) 2018  Stan Overgauw
    Modified by Sander Laarhoven

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<?php
$string = file_get_contents("../votes.json");
$votes = json_decode($string, true);

$vote_id = $_GET['id'];

if ($vote_id === 'new') {
    $title = 'Nieuwe Stemming';
    $vote = [
        "id" => false,
        "title" => "",
        "type" => "",
        "options" => [],
        "options_vertical" => [],
        "options_horizontal" => [],
        "status" => "closed"
    ];
} else {
    $title = 'Stemming Bewerken';
    $vote = @$votes[$vote_id];
    $vote['id'] = $vote_id;
    if ($vote === null) {
        die("Geen stemming gevonden met het opgegeven ID.");
    }
}

$type_selected_grid = '';
$type_selected_list = '';
$list_options_visibility = 'display: none;';
$grid_options_visibility = 'display: none;';
if ($vote['type'] === 'grid') {
    $type_selected_grid = 'selected';
    $grid_options_visibility = 'display: block;';
} else {
    $type_selected_list = 'selected';
    $list_options_visibility = 'display: block;';
}

$status_selected_open = '';
$status_selected_closed = '';
if ($vote['status'] === 'open') {
    $status_selected_open = 'selected';
} else {
    $status_selected_closed = 'selected';
}

?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.typekit.net/qvc3lcx.css">
    <link rel="stylesheet" href="/style.css">
    <title>Digitaal stemmen - Admin</title>
</head>
<body>
<nav class="navbar navbar-light syntax-header">
    <span class="navbar-brand" href="#">
        <img src="https://syntaxleiden.nl/img/logo-syntax.png" class="d-inline-block align-top" alt="">
        Digitaal Stemmen &dash; Admin
    </span>
</nav>
<div class="container">
    <div class="row">
        <div class="col">
            <p></p>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <form method="post" action="/admin/process_edit_vote.php">
                <input type="hidden" name="id" value="<?php echo $vote['id']; ?>">
                <a href="/admin/votes.php" class="btn btn-light float-left">Terug naar Stemmingen</a>

                <?php if ($vote['id']) { ?>
                <a href="/admin/delete_vote.php?id=<?php echo $vote['id']; ?>" onclick="return confirm('Weet je zeker dat je deze stemming wil verwijderen?')" class="btn btn-danger float-right">Verwijder</a>
                <?php } ?>

                <br><br><br>
                <h3><?php echo $title; ?></h3>
                <div class="form-group">
                    <label for="title">Stemming Naam</label>
                    <input type="text" class="form-control" name="title" value="<?php echo htmlspecialchars($vote['title']); ?>" placeholder="Stemming FooBarCo">
                </div>
                <div class="form-group">
                    <label for="status">Stemming Status</label>
                    <select name="status" class="form-control">
                        <option value="open" <?php echo $status_selected_open; ?>>Open</option>
                        <option value="closed" <?php echo $status_selected_closed; ?>>Gesloten</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="type">Stemming Type</label>
                    <select name="type" class="form-control">
                        <option value="list" <?php echo $type_selected_list; ?>>Lijst</option>
                        <option value="grid" <?php echo $type_selected_grid; ?>>Grid</option>
                    </select>
                </div>
                <div class="list-options form-group" style="<?php echo $list_options_visibility; ?>">
                    <label for="options">Lijst opties</label>
                    <div class="inputs">
                        <?php
                        if ($vote['id']) {
                            foreach($vote['options'] as $option) {
                                ?><input name="options[]" class="form-control" value="<?php echo $option; ?>"><?php
                            }
                        }
                        ?>
                        <input type="text" name="options[]" class="form-control" value="">
                    </div>
                    <a href="#" class="btn btn-add-option btn-sm btn-light">Optie Toevoegen</a>
                </div>
                <div class="grid-options form-group" style="<?php echo $grid_options_visibility; ?>">
                    <label for="options_vertical">Grid opties verticaal</label>
                    <div class="inputs">
                        <?php
                        if ($vote['id']) {
                            foreach($vote['options_vertical'] as $option) {
                                ?><input name="options_vertical[]" class="form-control" value="<?php echo $option; ?>"><?php
                            }
                        }
                        ?>
                        <input type="text" name="options_vertical[]" class="form-control" value="">
                    </div>
                    <a href="#" class="btn btn-add-option btn-sm btn-light">Optie Toevoegen</a>
                </div>
                <div class="grid-options form-group" style="<?php echo $grid_options_visibility; ?>">
                    <label for="options_horizontal">Grid opties horizontaal</label>
                    <div class="inputs">
                        <?php
                        if ($vote['id']) {
                            foreach($vote['options_horizontal'] as $option) {
                                ?><input name="options_horizontal[]" class="form-control" value="<?php echo $option; ?>"><?php
                            }
                        }
                        ?>
                        <input type="text" name="options_horizontal[]" class="form-control" value="">
                    </div>
                    <a href="#" class="btn btn-add-option btn-sm btn-light">Optie Toevoegen</a>
                </div>

                <button type="submit" class="btn btn-success float-right">Opslaan</button>
                <br><br><br><br>
            </form>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<script>
    $('select[name="type"]').on('change', function (event) {
        const selected_type = $('select[name="type"]').val()
        $('.grid-options, .list-options').hide()
        $('.'+selected_type+'-options').show()
    })
    $('.btn-add-option').on('click', function (event) {
        const inputToClone = $(event.target).parent().find('.inputs > input').attr('name')
        $(event.target).parent().find('.inputs').append('<input type="text" name="'+inputToClone+'" class="form-control">')
    })
</script>
</body>
</html>
