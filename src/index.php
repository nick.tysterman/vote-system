<!--
    Copyright (C) 2018  Stan Overgauw

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<?php
/**
 * Created by PhpStorm.
 * User: stan
 * Date: 6/27/18
 * Time: 4:20 PM
 */
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">

    <title>Syntax ALV - Digitaal Stemmen</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.typekit.net/qvc3lcx.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <nav class="navbar navbar-light syntax-header">
        <span class="navbar-brand" href="#">
            <img src="https://syntaxleiden.nl/img/logo-syntax.png" class="d-inline-block align-top" alt="">
            Digitaal Stemmen
        </span>
    </nav>
    <div class="container syntax-content">
        <div class="row">
            <div class="col">
                <p>
                    Welkom op de website voor het digitaal stemmen van <a href="https://syntaxleiden.nl" target="_blank">Studievereniging Syntax</a>. Alle stemmen zijn anoniem.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <form method="post" action="stem.php">
                    <?php
                    $string = file_get_contents("votes.json");
                    $votes = json_decode($string, true);
                    $first = array_keys($votes)[0];
                    echo "<input type='hidden' value='$first' name='vote_id'>\n";
                    ?>
                    <div class="form-group">
                        <?php
                        if (isset($_GET['error']) && ($_GET['error'] === 'token_not_found')) {
                        ?>
                        <div class="alert alert-danger" role="alert">
                            <strong>Oopsie Woopsie.</strong>&nbsp;&mdash;
                            De opgegeven token is niet geldig. Typefoutje, of dikke vingers?
                        </div>
                        <?php
                        }
                        ?>
                        <input
                            id="token"
                            class="form-control readable-token"
                            type="text"
                            aria-describedby="tokenHelp"
                            placeholder="Vul token in.."
                            name="token"
                            required maxlength="19"
                            autocomplete="off"
                            autocorrect="off"
                            autocapitalize="off"
                            spellcheck="false"
                        >
                        <p id="tokenHelp" class="form-text text-muted" style="color: #000 !important;">
                            Dit is je persoonlijke anonieme token. Deze token is niet te herleiden naar jou als
                            persoon tenzij je je token deelt met anderen.
                        </p>
                    </div>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="legalRequirements" name="disclaimer" required>
                        <label class="form-check-label" for="legalRequirements" style="color: black;">
                            <ul style="list-style:none;padding-inline-start: 0px;">
                                <li>Ik verklaar dat de ingevulde token van mij is;</li>
                                <li>ik een stemgerechtigd lid ben van <a href="https://syntaxleiden.nl" target="_blank">S.V. Syntax</a>;</li>
                                <li>ik volgens <a href="https://syntaxleiden.nl/statuten" target="_blank">artikel 16 van de statuten</a> niet meer dan eenmaal zal stemmen.</li>
                            </ul>
                        </label>
                    </div>
                    <button type="submit" class="btn btn-primary float-right">Begin Stemmen</button>
                </form>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="ux-helper.js"></script>
</body>
</html>
